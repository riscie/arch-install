# arch-install

my arch linux install scripts

## Usage

```bash
# within the arch iso (needs network first)

# load swiss keyboard layout
loadkeys de_CH-latin1

# enable ssh if you like
passwd
systectl start sshd.service
ip a

# install git and clone scripts
pacman -Sy && sudo pacman -S git --noconfirm
git clone https://gitlab.com/riscie/arch-install.git

# edit hostname and username and maybe partitioning first, then run the script
cd arch-install
./install.sh
```

### layout.sfdisk

-   created with fdisk and then exported (using O) to be used with sfdisk in a scripted way. [See this superuser.com answer.](https://superuser.com/a/1132834/156300)
