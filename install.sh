#!/bin/bash

# exit on first error
set -e

# riscies arch installation script

echo "Let's install arch"

# load swiss keymap
loadkeys de_CH-latin1

# activate ntp
timedatectl set-ntp true

# wipe all data :S
wipefs -a /dev/sda

# create partitions
sfdisk /dev/sda < layout.sfdisk

# create and mount encrypted root partition
cryptsetup -y -v luksFormat /dev/sda3
cryptsetup open /dev/sda3 cryptroot
mkfs.ext4 /dev/mapper/cryptroot
mount /dev/mapper/cryptroot /mnt

# preparing boot partition
mkfs.ext4 /dev/sda2

# prepare boot directory and mount it
mkdir /mnt/boot
mount /dev/sda2 /mnt/boot

# Install Arch Linux
pacstrap /mnt base base-devel linux linux-firmware zsh git nano grub os-prober intel-ucode efibootmgr dosfstools networkmanager

# fstab
genfstab -U /mnt > /mnt/etc/fstab

more /mnt/etc/fstab

# copy post-install
cp post-install.sh /mnt/
echo "chrooting into /mnt now. After that, please run post-install.sh"

# chrooting in
arch-chroot /mnt
