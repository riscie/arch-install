#!/bin/bash

# set timezone
ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# generate adjtime
hwclock --systohc

# adapt and generate locale
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen

# make keymap persisten
echo "KEYMAP=de_CH-latin1" >> /etc/vconsole.conf

# set hostname
hostname=riscie-x250
echo "$hostname" > /etc/hostname

# hosts configuration
cat <<EOT >> /etc/hosts
127.0.0.1	localhost
::1		localhost
127.0.1.1	$hostname.localdomain	$hostname
EOT

# adapt mkinitcpio
sed -i '/HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)/c\HOOKS=(base udev autodetect keyboard keymap modconf block encrypt filesystems fsck)' etc/mkinitcpio.conf

# creating a new initramfs
mkinitcpio -P

# set root password
passwd

# Enable nm
systemctl enable NetworkManager.service

# Install bootloader
grub-install --target=i386-pc /dev/sda

echo Edit /etc/default/grub and append your kernel options between the quotes in the GRUB_CMDLINE_LINUX_DEFAULT line:
echo GRUB_CMDLINE_LINUX_DEFAULT="quiet splash cryptdevice=UUID=<UUID-of-/dev/sda2>:cryptroot root=/dev/mapper/cryptroot"
echo Then run grub-mkconfig -o /boot/grub/grub.cfg
echo Now reboot and have fun...